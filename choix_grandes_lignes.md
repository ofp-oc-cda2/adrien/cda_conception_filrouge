[index repo](README.md)

# GPS - Gnu Personal Stats
## nom:
 - CalendarStats
 - **GnuPersonalStats (GPS)**
## Cadre d'utilisation
 - pouvoir noter des quantitées de ce qu'on veut dans un calendrier
 - en sortir des stats et des graphiques
 - que les données ne soit accessible que par le user (inverse des applications de comparaison de stats, comme il en existe pour les stats sportives,...)
## fonctionnalités
 - Création de compte
 - On donne un titre (+description optionnel) a une ou plusieurs choses que l'on veut suivre (produits de consommation, argent, distance dans le cadre de transport ou de sport, ... ) du moment que l'on peut les noter sous forme de nombres/jours (ou semaines ou mois, les jours vides devraient être remplit automatiquement par 0) positifs ou négatif, avec ou sans virgules.
 - on remplit :
    - manuellement: sur la date du jour avec possibilités de modif des jours passé (voir à venir?)
    - via une base existante (?à voir) par import de fichier sql et/ou ods.
 - interface:
    - page d'accueil du compte:
       - fonction ajout dans chaque champs créer
       - création de nouveaux champs
       - visu (rapport): on peut choisir d'afficher ou non chaque champs (checkbox) et une durée (de quand à quand). choix également de lire par jours,semaines,mois et années (avec addition et moyenne des chiffres). l'interface sort un tableau avec les données travaillés, et un graphique. voir pour mettre à cet endroit une fonction d'export du visu (rapport). idée: prévenir si une période est incomplète ou inexistante dans le cas ou on commence en février et qu'on affiche l'année, ou si un champ est remplit depuis moins longtemps qu'un autre,...)
          - Attention: que faire quand on visualise une période sur laquel une catégorie n'existe pas encore? 0 défault? faudra t-il créer pleins de jours vide => essayer de renvoyer 0 si non-existant, comme un jour sur période existante non-remplit mais sans ralonger inutilement des cases? **reponse: on ne crée des entrées que si on a un chiffre a y mettre (manuel ou depuis un import) les autres jours n'existe pas dans la bdd, ils sont suggéré existant et à 0.**
        - fonction de download de la totalité des données
        - fonction changement MDP => le client récup ses infos cryptées, les décryptes avec l'ancien MDP et les ré-encrypte avec le new tout en modifiant le hash de pass. **voir si réaliste**
        - fonction de suppression du compte



## spécifications
 - cryptage coté client?
 - doit pouvoir être utilisé dans un navigateur mobile et desktop
 - sous forme d'application sur divers support? (client desktop cross plateform, androïd, Ios,... )?
    - outils envisagés: Vue => Electron et Cordova.
 - sous forme d'api pour qui veut recoder un front? => si oui, crées une doc API public.

## BACK

  ### base de donnée

  - table user:
     - id (int) KEY
     - nom (varchar?)
     - mail (varchar?)
     - hash password (varchar, text?)
     - comment (text)
  - table champs:
     - id (int) KEY
     - id du user (int) RELATION
     - nom (varchar?)
     - comment (text)
     - premier jour du champ (id/ date comme noté dans table calendar) (date ou int?)
  - table calendar: (une pour tous ou une par user ou **une par champs => structure plus légère mais pleins de tables**)  chaque jours, une entrée, dans laquel on trouve: (**cas une par champ**)
     - id = date du jour (date ou int) KEY
     - id de la table champs correspondante (int) RELATION
     - nombre (valeur du jours) 
     - éventuellement, booléen pour savoir si remplit ou 0 par défaut

**!ATTENTION! si tables crypt => peut être stockage chaines de caractères longues uniquement**

  ### actions

Chaque entités (user, champs, calendar) peut être opéré selon les opération de bases CRUD.

 ## FRONT

  ### page de log
  - formulaire de connexion => renvoi à la page de compte.
  - formulaire de création de compte => renvoi à la page de compte.

 ### dashboard
  - se déconnecter
  - suppression du compte
  - change password
  - télécharger toutes les données de l'utilisateur.
  - création de champ
  - remplir des données dans un champ
     - manuellement
     - depuis un .ods, ...
  - data visualisation: **tableau et graph**
     - choisir la période
     - choisir le rythme d'affichage (day, week, month, year)
        - quand != day, afficher les périodes en additionnant les jours renseignés constituant la période.
     - choisir les champs affichées (checkbox)
     - option export tableau/visu
     - voir pour prévenir si champ inexistant sur tout ou partie de la durée choisit, ou mettre 0 par défaut simplement?

## pistes techniques

 - back node
    - bdd **nosql** ? se renseigner => ?
   ```json
      {
         "userId0":[{
            "name":"",
            "mail":"",
            "hash":"",
            "comment":"",
            "champs":[{
                  "champId1":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }],
                  "champId2":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }],
                  "champId3":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }]
            }]
         }],
         "userId1":[{
            "name":"",
            "mail":"",
            "hash":"",
            "comment":"",
            "champs":[{
                  "champId1":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }],
                  "champId2":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }],
                  "champId3":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }]
            }]
         }],
         "userId2":[{
            "name":"",
            "mail":"",
            "hash":"",
            "comment":"",
            "champs":[{
                  "champId1":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }],
                  "champId2":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }],
                  "champId3":[{
                     "name":"",
                     "comment":"",
                     "calendars":[{
                        "calendarId1":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId2":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId3":[{
                              "date":"",
                              "value":""
                        }],
                        "calendarId4":[{
                              "date":"",
                              "value":""
                        }]
                     }]
                  }]
            }]
         }]
      }
   ```
 - front vue => permet un export plus simple electron pour desktop (windows, mac et linux), cordova pour mobile (Ios Androïd et ?windows).

 [index repo](README.md)