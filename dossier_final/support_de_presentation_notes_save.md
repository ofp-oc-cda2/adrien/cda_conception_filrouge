# support_de_presentation_notes_save.md

 - oral: 40 min
 - entretiens technique : 45 min
 - entretiens final : 20 min

## slide 1

The idea of this application is a derivate of spreadsheet that allows to feed and view statistical fields. It can handle any numerical statistics (integer, decimal, positive or negative) and allows visualization in the form of daily, weekly, monthly or annual monitoring.

The interface proposes to login or create an account. When the user is logged, he is directed to a space where he can modify the information concerning his account and his data fields, or view the existing data.

The visualisation interface consist of a graphic, with dynamics controllers to manipulate this. These controllers elements allowing to manipulate a graph for visualizing the data by day, week, month and year, as well as to determine which data fields are visible, how many periods are displayed, and which is the last period displayed on a graph.

In summary, this application can be used for a lot of variety of purposes such as budget control, monitoring consumption, count of actions, … As long as it is quantifiable. It also allows you to easily overlay these data, in order to establish correlations or forecasts per example.

----

The idea of this application is a derivate of spreadsheet that allows to feed and view statistical fields. It can handle any numerical statistics (integer, decimal, positive or negative) and allows visualization in the form of daily, weekly, monthly or annual monitoring.

The visualisation interface consist of a graphic, with dynamics controllers to manipulate this.

In summary, this application can be used for a lot of variety of purposes such as budget control, monitoring consumption, count of actions, … As long as it is quantifiable. It also allows overlay these data, in order to establish correlations or forecasts per example.


## slide 2 : presentation de l'entreprise

Il y avait un projet (outil de communication+cloud) interne à l’entreprise, mais abandonné
donc le projet fil rouge remplace.
Cependant des compétences acquises en formation ont été utilisé dans des projets pro (WP et outils de travail) , perso et benevoles

d’édition à compte d’auteur => assure seulement la partie technique de l'édition et de la diffusion. C’est l'auteur qui paie les frais d'impression et de publicité

## slide 3 analyse du besoin, context, livrable

Problème feuille de calcul :
 - multiples graphiques
 - définition manuel des plages de données pour les graphiques
 - ram

Adaptation stack technique en fonction besoins et livrables

## slide 4 : gestion de projet

- comme dit dans les contraintes, développer seul et temps
- Planning et suivis
   - README.md
   - sinon kanban github avec dev ou trello
 - objectifs qualité
   - Eslint
   - techno cité plus haut ( selenium, insomnia ) 

## slide 5 : Conception des données et fonctions

Page conception : parler de :
 - Forme des données (ici MCD simplifié)
   - Foreign key
   - Séparation dans le schéma du calendar
 - Séquences (frontend, backend, BDD) vite fait

## slide 6 : maquette

 - Utilisation de figma
 - plan de navigation dans l'interface
 - parle du layaout/UIKit quasar, adaptation maquette/réel

## slide 7 : présentation de l'élément le + significatif de l'interface

 - présentation de l’interface
 - présentation schéma code

## slide 8 : jeu d'essai

insomnia couvre toutes les requêtes API. mocha et selenium non.

 - insomnia
   - quasi intégralité de l'API développer sans interface web (uniquement insomnia) sauf cas spécifiques (refresh tocken)
   - tests cas de figures: tentative injections malveillantes, mauvaises données, same user name, ...
 - mocha
   - test functions, routes API
   - chemin (insomnia automatique): user create, get tocken & id, user delete.
 - selenium
   - test login bad password, test login true.
   - parle des ID dynamiques.

## slide 10 : exemple de recherche effectuée à partir de site anglophone

 - **syntaxe de recherche :** 
   - seuls les "mots clefs" sont pris en compte
   - plus un mot clef est au début de requête, plus il est fortement pris en compte
   - guillemets pour forcer une occurence exact
   - moins et plus devant mot/chaine pour suppr/mettre en avant
   - -site: pour cibler un domaine
   - filetype:pdf pour type de fichier
   - AND (&) OR (|) NOT (-)
Url osint resource : https://ozint.eu/fiche-pedagogique/moteurs-de-recherche/

url request: https://duckduckgo.com/?q=%22jsonwebtoken%22+%22nodeJs%22+how+implement&t=ffab&ia=webexport de compétence
Url site : https://www.digitalocean.com/community/tutorials/nodejs-jwt-expressjs

## slide 11 : synthèse et conclusion

 - Acquisition de compétences
   - satisfaction
     - découverte architecture API
     - découverte languages et frameworks (vue, quasar, noSql/mongoDB/mongoose/mongo-express, nodeJs, docker/docker-compose, JWT, stores/localStorage, ...
     - export des nouvelles compétences (travail, projets perso)
   - dificultées
     - (?) contraintes de temps?
 - Suite à donner au projet
   - implémentations ultérieurs (prod?, import/export json/CSV, interface saisie UX++)
 - Recherche de boulot
   - pistes utilisant des compétences
 - (?) remerciements












# prépa exam

 - bien habillé
 - max 10 min de demo de l'app
 - attention aux tics de languages

 - savoir expliquer patterne MVC?
 - ORM object relationnal mapping => traitement éléments de BDD comme objets
 - différence objet/class = objet est une instance de class (type de donnée pour fabriquer des objets); class: méthodes, propriété. visibilité: private/public/protected
   - private: uniquement dans la class
   - public: exterieur
   - protected: visible uniquement dans class hérité
 - expliquer les failles de sécuritées
   - XSS (cross site scripting) via formulaire, url, ... framework front moderne échape, partie back: systeme mongoose
   - injection SQL (requêtes préparer, bindparams) => voir pour mongoDB (ORM mongoose échappe?)
   - CSRF Cross-site Request Forgery
 - différence let, const et var (scope)
 - conditions, boucles, fonctions fléchés, promise
 - SQL:
   - jointure interne: ressort les entrées correspondantes uniquement (inner join ou join)
   - jointure externe: ressort tout le monde, mais:
     - **left join**: liste toute la table de gauche, que les correspondante à droite
     - **right join**: liste toute la table de droite, que les correspondante de droite

 **plus tu rentre dans les détailles, plus tu ammène le jury à posé des question dans le détail, plus tu risque de te prendre les pieds dans le tapis**

si on connait pas réponse à question: je pense que, dans le cadre pro j'irait voir sur internet, ... plutot que je sais pas

si question sur avenir pro, surtout pas dire "je prend une période de repos"

















# glossaire

## API

**Application Programing Interface**: façade par laquelle un logiciel offre des services à d'autres logiciels.

## XSS

**cross-site scripting**

injecter des données, par input ou URL (ou tout contenu de requête http?). Si ces données arrivent telles quelles dans la page web transmise au navigateur (par les paramètres d'URL, un message posté…) sans avoir été vérifiées, alors il existe une faille : on peut s'en servir pour faire exécuter du code malveillant en langage de script (du JavaScript le plus souvent) par le navigateur web qui consulte cette page. 

## CSRF

**cross-site request forgery**: transmettre à un utilisateur authentifié une requête HTTP falsifiée qui pointe sur une action interne au site.

## ORM

**object-relational mapping**: type de programme informatique qui se place en interface entre un programme applicatif et une base de données relationnelle.

**mongoose est un ORM**

## Object

conteneur contentant un ensemble de clefs-valeurs

## array

est une structure de données représentant une séquence finie d'éléments auxquels on peut accéder efficacement par leur position, ou indice, dans la séquence

## map

objet itérable (boucle), dont les clefs peuvent être compté (size), meilleurs perf pour nombreux ajout/suppr de clefs, ordre de clef selon ordre insertion

## class

**Une class contiens méthode et propriété.**

**un objet est une instance de class**

Une classe est une description des caractéristiques d'un ou de plusieurs objets. Chaque objet créé à partir de cette classe est une instance de la classe en question

 - private: uniquement dans la class
 - public: exterieur
 - protected: visible uniquement dans class hérité

```JS
class Point {
    
    constructor(x, y) {
        this._x = x;
        this._y = y;
    }
    
    getX() {
        return this._x;
    }
    
    getY() {
        return this._y;
    }
    
    isOrigin() { 
        return this._x === 0 && this._y === 0; 
    }
    
    translate(pt) {
        return new Point(this._x + pt._x, this._y + pt._y);
    }
    
}
```

## let, const, var

```JS
// 1. Déclaration dans un bloc

if (true) {            // début du bloc
    var maVariable1;   // déclaration de la variable
    let maVariable2;   // déclaration de la variable
    const maVariable3; // déclaration de la variable
}                      // fin du bloc mais pas de la portée de maVariable1

alert(maVariable1);    // ne soulève pas d'erreur
alert(maVariable2);    // erreur : la variable est hors de sa portée
alert(maVariable3);    // erreur : la variable est hors de sa portée

// 2. Déclaration dans une fonction

function maFunction() { // début de la fonction
    var maVariable4;    // déclaration de la variable
    let maVariable5;    // déclaration de la variable
    const maVariable6;  // déclaration de la variable
}                       // fin de la fonction et de la portée des variables

alert(maVariable4);     // erreur : la variable est hors de sa portée
alert(maVariable5);     // erreur : la variable est hors de sa portée
alert(maVariable6);     // erreur : la variable est hors de sa portée
```

## SQL

**Structured Query Language**: language à destination des bases de données relationnels.

## noSQL

**Not Only Structured Query Language**: SGBDD taillé pour les structure logicielle distribuée (fonctionnant avec des agrégats répartis sur différents serveurs permettant des accès et modifications concurrentes )

## CAP (théorème)

 - **Consistency (Cohérence)** : Une donnée n'a qu'un seul état visible quel que soit le nombre de replicas
 - **Availability (Disponibilité)** : Tant que le système tourne (distribué ou non), la donnée doit être disponible
 - **Partition Tolerance (Distribution)** : Quel que soit le nombre de serveurs, toute requête doit fournir un résultat correct

 SQL = AC (Availability + Consistency)
 MongoDB = CP (Consistency + Partition Tolerance)

## ACID (propriétés BDD SQL)

## BASE (propriétés BDD NoSQL)

## MVC

**Modèle-Vue-Contrôleur**: motif d'architecture logicielle destiné aux interfaces graphiques.
 - **Model**: contient les données à afficher.
 - **View**: contient la présentation de l'interface graphique.
 - **Controller**: contient la logique concernant les actions effectuées par l'utilisateur.

## arrow function

généralement fonctions anonymes. syntaxe: 
```JS
(param, param) => {
   // instructions
}

let name = (param, param) => {
  // instructions
}
```
## REST (API)

**REpresentational State Transfer**:
 - utilisation des verbes HTTP: POST, GET, PUT, DELETE (CRUD)
 - une url par type d'opération, incluant les identifiants (ex GET https://api.site.fr/user/:id)
 - permet une communication entre des languages très différents (python, C++, ...)



















































## API

## XSS

## CSRF

## ORM

## Object

## array

## map

## class

## let, const, var

## SQL

## noSQL

## CAP (théorème)

## BASE

## ACID

## MVC

## arrow function

## REST (API)

