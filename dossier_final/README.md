# notes de construction du dossier

exemple primaire: Oswald/plan obligatoire

## features and tips md to pdf

 - outil d'export: [Markdowd PDF](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf)
 - sauts de pages: `<div style="page-break-after: always;"></div>` ou `<div class="page"/>` (doc plugin)
 - internal link (for table of content): exemple: `[Liste des compétences du référentiel couvertes par le projet](#listCompetencesRef)` and `## Liste des compétences du référentiel couvertes par le projet <a name="listCompetencesRef"></a>`

 ## resumer en français

L’application GnuPersonalStats se veut un dérivé de feuille de calcul permettant d’alimenter et de visionner des champs statistiques. Celle-ci s’accommode de toutes statistiques numérique (entière ou décimal, positif ou négatif), et permet un suivi journalier, hebdomadaire, mensuel ou annuel.

L’interface propose de se connecter ou de créer un compte. Une fois connecter, l’utilisateur est diriger vers sont espace, proposant soit de modifier les informations concernant son compte et ces champs de données, soit de visualisé les données existantes. Toutes ces vues et actions sont regrouper dans des pop-up.

L’interface de visualisation consiste en un graphique, dont l’affichage dynamique est relier à des input permettant de manipuler le graphique. Ces éléments de contrôles permettent de visualisé les valeurs enregistré par jours, semaines, mois et années, ainsi que de déterminer quels champs de données sont visibles, combien de périodes sont afficher, et quel est la dernière période afficher.

En résumé, cette application peut être utilisé pour des usages assez divers comme une maîtrise de budget, le suivi d’une consommation, d’une action… du moment que cela est quantifiable. Elle permet par ailleurs facilement de superposé ces données, afin d’établir des corrélations, des prévisions.

## translate final version (by depple)

L'idée de cette application est un dérivé de tableur qui permet d'alimenter et de visualiser des champs statistiques. Il peut traiter n'importe quelle statistique numérique (entière, décimale, positive ou négative) et la visualiser sous forme de suivi quotidien, hebdomadaire, mensuel ou annuel.

L'interface propose de se connecter ou de créer un compte. Lorsque l'utilisateur est connecté, il est dirigé vers un espace où il peut modifier les informations concernant son compte et ses champs de données, ou visualiser les données existantes. Ces visualisations et actions se font dans des fenêtres pop-up.

L'interface de visualisation consiste en un graphique, avec des contrôleurs dynamiques pour le manipuler. Ces éléments de contrôleurs permettent de manipuler un graphique pour visualiser les données par jour, semaine, mois et année, ainsi que de déterminer quels champs de données sont visibles, combien de périodes sont affichées, et quelle est la dernière période affichée sur un graphique.

En résumé, cette application peut être utilisée pour une grande variété d'objectifs tels que le contrôle du budget, le suivi de la consommation, le comptage des actions, ... Tant qu'il est quantifiable. Elle permet également de superposer facilement ces données, afin d'établir des corrélations ou des prévisions par exemple.

## build non-aboutis apk

Install androïd studio :
 $ sudo snap install android-studio --classic 
android-studio 2021.3.1.17 par Snapcrafters installé

 $ export ANDROID_SDK_ROOT=/home/x/Android/Sdk

quasar build -m capacitor -T android

 .d88888b.
d88P" "Y88b
888     888
888     888 888  888  8888b.  .d8888b   8888b.  888d888
888     888 888  888     "88b 88K          "88b 888P"
888 Y8b 888 888  888 .d888888 "Y8888b. .d888888 888
Y88b.Y8b88P Y88b 888 888  888      X88 888  888 888
 "Y888888"   "Y88888 "Y888888  88888P' "Y888888 888
       Y8b


 Build mode............. capacitor
 Pkg quasar............. v2.11.1
 Pkg @quasar/app-vite... v1.1.3
 Pkg vite............... v2.9.15
 Debugging.............. no
 Publishing............. no
 Packaging mode......... gradle

 App • Cleaned build artifact: "/home/x/Documents/CDA/gps_quasar/dist/capacitor"
 App •  WAIT  • Compiling of Capacitor UI with Vite in progress...
Use of eval is strongly discouraged, as it poses security risks and may cause issues with minification
Use of eval is strongly discouraged, as it poses security risks and may cause issues with minification

 App •  DONE  • Capacitor UI compiled with success • 6413ms

 UI files build summary:
 ╔══════════════════════════════════╤═══════════╗
 ║                            Asset │      Size ║
 ╟──────────────────────────────────┼───────────╢
 ║ assets/ErrorNotFound.60c1a22d.js │   0.58 KB ║
 ║     assets/IndexPage.679d99a0.js │   3.76 KB ║
 ║    assets/MainLayout.983e9cc8.js │  20.57 KB ║
 ║         assets/QPage.284d650c.js │   0.87 KB ║
 ║       assets/account.7b5abba9.js │ 452.29 KB ║
 ║       assets/apropos.4b4e072d.js │   2.87 KB ║
 ║        assets/format.34e9cd7d.js │   7.42 KB ║
 ║         assets/hello.9eacd782.js │   2.60 KB ║
 ║          assets/i18n.1ea3639b.js │  26.38 KB ║
 ║         assets/index.49102a4d.js │ 225.99 KB ║
 ║    assets/use-quasar.eafc523e.js │   0.08 KB ║
 ╟──────────────────────────────────┼───────────╢
 ║    assets/IndexPage.52531915.css │   0.17 KB ║
 ║   assets/MainLayout.c8cfcbd1.css │   0.08 KB ║
 ║      assets/account.73560ee6.css │   0.15 KB ║
 ║      assets/apropos.84d89835.css │   0.18 KB ║
 ║        assets/index.dbdcd91d.css │ 195.98 KB ║
 ╟──────────────────────────────────┼───────────╢
 ║                       index.html │   0.85 KB ║
 ╚══════════════════════════════════╧═══════════╝
 
 App • Updated src-capacitor/package.json
 App • Updated capacitor.config.json
 App • Running "/home/x/Documents/CDA/gps_quasar/src-capacitor/node_modules/@capacitor/cli/bin/capacitor sync android" in /home/x/Documents/CDA/gps_quasar/src-capacitor

✔ Copying web assets from www to android/app/src/main/assets/public in 39.69ms
✔ Copying native bridge in 599.64μp
✔ Copying capacitor.config.json in 604.68μp
✔ copy in 46.77ms
✔ Updating Android plugins in 489.19μp
  Found 0 Capacitor plugins for android:
✔ update android in 5.59ms
Sync finished in 0.057s

 App • Updated capacitor.config.json
 App • Building Android app...
 App • [sync] Running "./gradlew assembleRelease" in /home/x/Documents/CDA/gps_quasar/src-capacitor/android


> Configure project :app
google-services.json not found, google-services plugin not applied. Push Notifications won't work

...
...


> Task :capacitor-android:compileReleaseJavaWithJavac
Note: Some input files use or override a deprecated API.
Note: Recompile with -Xlint:deprecation for details.

BUILD SUCCESSFUL in 1m 16s
85 actionable tasks: 85 executed
 App • Added build artifact "/home/x/Documents/CDA/gps_quasar/dist/capacitor"

 Build succeeded

 Build mode............. capacitor
 Pkg quasar............. v2.11.1
 Pkg @quasar/app-vite... v1.1.3
 Pkg vite............... v2.9.15
 Debugging.............. no
 Publishing............. no
 Packaging mode......... gradle
 Browser target......... es2019|edge88|firefox78|chrome87|safari13.1
 =======================
 Output folder.......... /home/x/Documents/CDA/gps_quasar/dist/capacitor

 Tip: "src-capacitor" is a Capacitor project folder, so everything you know
      about Capacitor applies to it. Quasar CLI generates the UI content
      for "src-capacitor/www" folder and then either opens the IDE or calls
      the platform's build commands to generate the final packaged file.

 Tip: Feel free to use Capacitor CLI ("yarn capacitor <params>" or
      "npx capacitor <params>") or change any files in "src-capacitor", except
      for the "www" folder which must be built by Quasar CLI.
Sortie : un paquet APK
