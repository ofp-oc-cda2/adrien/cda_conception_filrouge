Adrien RAYMOND
<div style="height: 35vh;" />

<h1 style="text-align: center; font-size: xx-large;">DOSSIER DE PROJET</h1>

  <!-- # DOSSIER DE PROJET -->

  ## Gnu Personal Stats

<div style="page-break-after: always;"></div>

## table des matières

1. [Liste des compétences du référentiel couvertes par le projet](#listCompetencesRef)
2. [Resumer du projet](#resumeProject)
3. [Cahier des charges](#cahierCharges)
4. [Gestion de projet](#gestionProjet)
5. [Spécifications fonctionnelles](#specificationsFonctionnelles)
6. [Spécifications techniques](#specificationsTechniques)
7. [Réalisation](#realisation)
8. [jeu d’essai](#jeuEssai)
9. [Description de la veille](#descriptionVeille)
10. [Recherche pour les besoin du projet](#searchForProject)
11. [Spécifications fonctionnelles](#specificationsFonctionnelles)

<div style="page-break-after: always;"></div>

## Liste des compétences du référentiel couvertes par le projet <a name="listCompetencesRef"></a>

<div style="page-break-after: always;"></div>

## Resumer du projet <a name="resumeProject"></a>

<!-- in english -->

<div style="page-break-after: always;"></div>

## Cahier des charges <a name="cahierCharges"></a>

<div style="page-break-after: always;"></div>

## Gestion de projet <a name="gestionProjet"></a>

<div style="page-break-after: always;"></div>

## Spécifications fonctionnelles <a name="specificationsFonctionnelles"></a>

<div style="page-break-after: always;"></div>

## Spécifications techniques <a name="specificationsTechniques"></a>

<div style="page-break-after: always;"></div>

## Réalisation <a name="realisation"></a>

<div style="page-break-after: always;"></div>

## jeu d’essai <a name="jeuEssai"></a>

<div style="page-break-after: always;"></div>

## Description de la veille <a name="descriptionVeille"></a>

<div style="page-break-after: always;"></div>

## Recherche pour les besoin du projet <a name="searchForProject"></a>

