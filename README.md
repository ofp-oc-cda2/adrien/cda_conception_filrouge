# CDA_conception_filRouge

## descriptif du projet

Cette application doit permettre de visualisé des données et leurs évolution dans le temp. Cela s'applique à toutes données numériques (entiers ou décimaux, positif ou négatif) lorsque une donnée = 1 jour, une semaine, un mois ou 1 an.

d'un coté, on entre les données, de l'autre on les visualises sous forme de tableaux et de graphes.

Idéalement, les données seraient cryptées coté client.

## lien vers le vrac:

[choix_grandes_lignes.md](choix_grandes_lignes.md)

## fichier propre: (in progress)

[conception_v1](conception_v1/index.md)

## exemples requêtes api

[insomnia collection](./Insomnia_2022-11-10.json)

# CDA red thread design

## project description

this app must allow viewing datas and his evolution in time. it's for all numeric data (integers or decimals, positive or negative) when one data = one day, week, month or year.

on one side, we enter data, on the other, we visualise them in table and graphic.

in the best of worlds, data would be encrypted in client side.

## link to bulk sheet:

[choix_grandes_lignes.md](choix_grandes_lignes.md)

## link to tidy sheet:

[conception_v1](conception_v1/index.md)

## exemple api request

[insomnia collection](./Insomnia_2022-11-10.json)