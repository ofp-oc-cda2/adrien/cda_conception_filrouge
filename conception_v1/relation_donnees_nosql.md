[index repo](../README.md)

## relations données: (version nosql json)

```mermaid
classDiagram
    direction TB
    user --|> champ : 0,n
    user <|-- champ : 1
    user --o champ : champ.userId<br>=== user.id
    champ --|> calendar : 0,n
    champ <|-- calendar : 1
    champ --o calendar : champ.id===<br>calendar.champId

    class user{
        +name string
        +mail string
        +hash string
        #comment string}
    class champ{
        #name string
        #color string
        #comment string}
    class calendar{
        #date date
        #value float number}

```
exemple de structure:

```json
{
    "_id": "ObjectId('6231c10ade3a20b9d40972c3')",
    "userName": "",
    "mail":"",
    "hash":"",
    "comment":"",
    "champs":[{
        "champName":[{
            "color":"",
            "comment":"",
            "calendars":
                [{                
                    "date":"",
                    "value":""
                }]
                [{                
                    "date":"",
                    "value":""
                }]
                [{                
                    "date":"",
                    "value":""
                }]
            }]
        }],
        "champName":[{
            "color":"",
            "comment":"",
            "calendars":
                [{                
                    "date":"",
                    "value":""
                }]
                [{                
                    "date":"",
                    "value":""
                }]
                [{                
                    "date":"",
                    "value":""
                }]
            }]
        }],
        "champName":[{
            "color":"",
            "comment":"",
            "calendars":
                [{                
                    "date":"",
                    "value":""
                }]
                [{                
                    "date":"",
                    "value":""
                }]
                [{                
                    "date":"",
                    "value":""
                }]
            }]
        }]
    }]
}
```

### séquençage des opérations

### operations on user
```mermaid

sequenceDiagram

    participant c as client (vue)
    participant b as backend (node)
    participant d as data

    rect rgba(50, 50, 255, 0.6)
        note over d,c:put (INSERT)
        c ->> b: /user/<br>PUT<br>{name,mail,password_hash,comment}
        b ->> d: verif if name exist
        d ->> b: not exist
        b ->> b: send mail
        b ->> c: check your mail please
        c ->> b: /user/mailcode<br>PUT<br>{mailCode}
        b ->> d: create user
    end

    rect rgba(50, 255, 50, 0.6)
        note over d,c:connect
        c ->> b: /user/login<br>{name,password_hash}
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: search all data of user
        d ->> c: if(true)<br>{user object entier}
        c ->> c: stoquage tocken<br>vue dashboard
    end

    rect rgba(255, 200, 100, 0.6)
        note over d,c: disconnect
        c ->> c: delelte tocken<br>vue acceuil<br>with disconect message
    end


    rect rgba(150, 150, 150, 0.6)
        note over d,c:update
        c ->> b: /user/<br>UPDATE<br>[tocken{password_hash, name},<br>{password_hash, name, comment}]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: if tocken = true => update
        d ->> c: it's ok
        c ->> c: vue dashboard update
    end

    rect rgba(150, 0, 0, 0.6)
        note over d,c:DELETE
        c ->> b: /user/<br>DELETE<br>[tocken{password_hash, name}]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: DELETE user <br>and user>champs <br>and user>champs>calendar
        b ->> c: it's ok
        c ->> c: vue accueil<br>with delete message
    end
```

### operations on champ


```mermaid

sequenceDiagram

    participant c as client (vue)
    participant b as backend (node)
    participant d as data

    rect rgba(50, 50, 255, 0.6)
        note over c,d:PUT (INSERT)
        c ->> b: /champ<br>INSERT<br>[tocken{password_hash,name}<br>newName, newComment, newColor]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: INSERT [newName, newComment, newColor]
        b ->> c: INSERT champ<br>message INSERT ok
        c->> c: update vue
    end

    rect rgba(150, 150, 150, 0.6)
        note over c,d:UPDATE
        c ->> b: /champ<br>UPDATE<br>[tocken{password_hash,name}<br>id, newName, newComment, newColor]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: verif id exist
        d ->> b: TRUE
        b ->> d: UPDATE in id [newName, newComment, newColor]
        b ->> c: UPDATE champ id<br>message update ok
        c ->> c: update vue
    end

    rect rgba(150, 0, 0, 0.6)
        note over c,d:DELETE
        c ->> b: /champ<br>DELETE<br>[tocken{password_hash,name}<br>id]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: verif id exist
        d ->> b: TRUE
        b ->> d: DELETE in champ id<br>(et son contenu dont calendar)
        b ->> c: delete ok
        c->> c: update vue
    end
```

### operations on calendar

```mermaid

sequenceDiagram

    participant c as client (vue)
    participant b as backend (node)
    participant d as data

    rect rgba(50, 50, 255, 0.6)
        note over c,d:put (INSERT)
        c ->> b: /calendar<br>INSERT<br>[tocken{password_hash,name}<br>champ, date, value]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: verif champ.id>calendar.date not exist in user
        d ->> b: TRUE
        b ->> d: INSERT into champ<br>[date, champId, value]
        b ->> c: INSERT ok
        c->> c: update vue
    end

    rect rgba(150, 150, 150, 0.6)
        note over c,d:UPDATE
        c ->> b: /calendar<br>UPDATE<br>[tocken{password_hash,name}<br>id, champId, date, value]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: verif champ.id>calendar.date exist in user
        d ->> b: TRUE
        b ->> d: UPDATE into <br> champ.id>calendar.date <br> [date, value]
        b ->> c: UPDATE calendar<br>message UPDATE ok
        c->> c: update vue
    end

    rect rgba(150, 0, 0, 0.6)
        note over c,d:DELETE
        c ->> b: /calendar<br>DELETE<br>[tocken{password_hash,name}<br>champId.calendarId]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: if exist => champId.calendarId
        d ->> b: TRUE
        b ->> d: DELETE champId.calendarId
        b ->> c: message DELETE ok
        c->> c: update vue
    end
```

### operations on calendar spécial

```mermaid

sequenceDiagram

    participant c as client (vue)
    participant b as backend (node)
    participant d as data

    rect rgba(50, 50, 255, 0.6)
        note over c,d:PUT (INSERT) with upload CSV
        c ->> b: /calendar<br>INSERT<br>[tocken{password_hash,name}<br>champ, date, value]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: verif champ.id>calendar.date not exist in user
        d ->> b: TRUE
        b ->> d: INSERT into champ<br>[date, champId, value]
        b ->> c: INSERT ok
        c->> c: update vue
    end
```

[index repo](../README.md)