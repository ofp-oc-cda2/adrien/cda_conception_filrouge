[index repo](../README.md)

# **V1 CONCEPTION**

d'apres les directives: [section livrable](https://gitlab.com/ofp-oc-cda2/ressources/-/blob/main/Formation/Fil%20rouge.md#livrables)

d'après l'ébauche: [choix_grandes_lignes](../choix_grandes_lignes.md)

# sommaire

 - [Cas d'utilisation](#cas-dutilisation)
 - [Modèle de données](#modèle-de-données)
    - [représentation du stoquage des données](#représentation-du-stoquage-des-données)
    - [relations données (version sql relationnel)](#relations-données-version-sql-relationnel)
       - [operations on user](#operations-on-user)
       - [operations on champ](#operations-on-champ)
       - [operations on calendar](#operations-on-calendar)
 - [Spécifications fonctionnelles](#spécifications-fonctionnelles)
 - [ Maquette](#maquette)

# Cas d'utilisation

note: les cas suivant peuvent s'appliquer aussi bien pour un visuel journalier, hebdomadaire, mensuel ou annuel.

 - suivre une consommation/habitude (nombre d'allé-retour au fast-food du coin de la rue, nombre d'achat d'un produit de consommation courante définit, ...)
 - suivre des dépenses (total des dépenses en magasin, ou d'un magasin/article en particulier, argent dépensé por faire le plein,...)
 - visualisé une pratique sportive (nombre de Km marché par jours, nombre de pompes effectuer par jours, ...)
 - on peut supposé que l'interface permette suffisamment de libertés pour permettre des usages imprévu lors de la conception.

# Modèle de données

**notes:**
 - réfléchir à la possibilitée d'une utilisation hors connexion.
 - voir por fonctions dans diagram de class

## représentation du stoquage des données:

![](diagrammeBdd.png)

## relations données:

 - [link version sql relationnel](relation_donnees_relationnel.md)

 - [link version nosql json](relation_donnees_nosql.md)

# Spécifications fonctionnelles

## dependances

|bloc |dependances  | calendrier <br> prévisionnel | check |
|:--- | :--- | :--- | :---: |
|cahier des charges||2022-02-18| [ ] |
|maquette|cahier des charges|2022-02-18| [ ] |
|BDD|cahier des charges|2022-03-18| [ ] |
|backend(node)|cahier des charges, BDD|2022-04-15| [ ] |
|client app (vue.js)|cahier des charges, backend(node)|2022-05-13| [ ] |
|export web/desktop/mobile|client app(vue.js)|2022-06-17| [ ] |

## dead line/organisation

|event |date | 
|:--- | :--- |
|**rendu et présentation**|**2023/02/20 (à confirmer)**|

# Maquette

 - [local fig file](../app.fig)
 - [fig file on figma.com](https://www.figma.com/file/NTKjub7PXvzxVM1hEfyYxG/app-CDA-GPS-V2?t=lEKFbZhK9tQj9gRo-1)

[index repo](../README.md)