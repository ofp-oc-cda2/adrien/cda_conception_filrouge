[index repo](../README.md)

## relations données: (version sql relationnel)

```mermaid
classDiagram
    direction TB
    user --o champ : 0,n
    user <|-- champ : 1
    champ --o calendar : 0,n
    champ <|-- calendar : 1

    class user{
        <<data object>>
        +id int
        +name string
        +mail string
        +hash string
        #comment string}
    class champ{
        <<data object>>
        +id int
        +userId int
        #color string
        #name string
        #comment string}
    class calendar{
        <<data object>>
        +id int
        #date date
        +champId int
        #value float number}

```


### operations on user
```mermaid

sequenceDiagram

    participant c as client (vue)
    participant b as backend (node)
    participant d as data>user

    rect rgba(50, 50, 255, 0.6)
        note over d,c:put (INSERT)
        c ->> b: /user/<br>PUT<br>{name,mail,password_hash,comment}
        b ->> d: verif if name or mail exist
        d ->> b: not exist
        b ->> b: send mail
        b ->> c: check your mail please
        c ->> b: /user/mailcode<br>PUT<br>{mailCode}
        b ->> d: create user
    end

    rect rgba(50, 255, 50, 0.6)
        note over d,c:connect
        c ->> b: /user/login<br>{name,password_hash}
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: search all data of user
        d ->> c: if(true)<br>{user=>champs=>calendars}
        c ->> c: stoquage tocken<br>vue dashboard
    end

    rect rgba(255, 200, 100, 0.6)
        note over d,c: disconnect
        c ->> c: delelte tocken<br>vue acceuil<br>with disconect message
    end


    rect rgba(150, 150, 150, 0.6)
        note over d,c:update
        c ->> b: /user/<br>UPDATE<br>[tocken{password_hash, name},<br>{password_hash, name, comment}]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> d: if tocken = true => update
        d ->> c: it's ok
        c ->> c: vue dashboard update
    end

    rect rgba(150, 0, 0, 0.6)
        note over d,c:DELETE
        c ->> b: /user/<br>DELETE<br>[tocken{password_hash, name}]
        b ->> d: verif tocken
        d ->> b: tocken = TRUE
        b ->> b: mail to confirm <br> (with new mail)
        b ->> c: check your mail please
        c ->> b: /user/mailcode<br>DELETE<br>
        b ->> d: if mailcode = true => DELETE user <br>and user>champs <br>and user>champs>calendar
        b ->> c: it's ok
        c ->> c: vue accueil<br>with delete message
    end
```

### operations on champ

```mermaid

sequenceDiagram

    participant c as client (vue)
    participant b as backend (node)
    participant duser as data>user
    participant dchamp as data>user>champ
    participant dcalendar as data>user>champ>calendar

    rect rgba(50, 50, 255, 0.6)
        note over c,dcalendar:put (INSERT)
        c ->> b: /champ<br>INSERT<br>[tocken{password_hash,name}<br>newName, newComment]
        b ->> duser: verif tocken
        duser ->> b: tocken = TRUE
        b ->> dchamp: INSERT [newName, newComment, newColor]
        b ->> c: INSERT champ<br>message INSERT ok
        c->> c: update vue
    end

    rect rgba(150, 150, 150, 0.6)
        note over c,dcalendar:UPDATE
        c ->> b: /champ<br>UPDATE<br>[tocken{password_hash,name}<br>id, newName, newComment, newColor]
        b ->> duser: verif tocken
        duser ->> b: tocken = TRUE
        b ->> dchamp: verif id exist
        dchamp ->> b: TRUE
        b ->> dchamp: UPDATE in id [newName, newComment, newColor]
        b ->> c: UPDATE champ id<br>message update ok
        c->> c: update vue
    end

    rect rgba(150, 0, 0, 0.6)
        note over c,dcalendar:DELETE
        c ->> b: /champ<br>DELETE<br>[tocken{password_hash,name}<br>id]
        b ->> duser: verif tocken
        duser ->> b: tocken = TRUE
        b ->> dchamp: verif id exist
        dchamp ->> b: TRUE
        b ->> dchamp: DELETE in champ id
        b ->> dcalendar: DELETE in calendar where champId
        b ->> c: DELETE champ id<br>message DELETE ok
        c->> c: update vue
    end
```

### operations on calendar

```mermaid

sequenceDiagram

    participant c as client (vue)
    participant b as backend (node)
    participant duser as data>user
    participant dchamp as data>user>champ
    participant dcalendar as data>user>champ>calendar

    rect rgba(50, 50, 255, 0.6)
        note over c,dcalendar:put (INSERT)
        c ->> b: /calendar<br>INSERT<br>[tocken{password_hash,name}<br>date, champId, value]
        b ->> duser: verif tocken
        duser ->> b: tocken = TRUE
        b ->> dchamp: verif champ.id exist in user
        dchamp ->> b: TRUE
        b ->> dcalendar: verif user>champ>calendar.date not exist
        dcalendar ->> b: confirm (else=update?)
        b ->> dcalendar: INSERT [date, champId, value]
        b ->> c: INSERT calendar<br>message INSERT ok
        c->> c: update vue
    end

    rect rgba(150, 150, 150, 0.6)
        note over c,dcalendar:UPDATE
        c ->> b: /calendar<br>UPDATE<br>[tocken{password_hash,name}<br>id, champId, date, value]
        b ->> duser: verif tocken
        duser ->> b: tocken = TRUE
        b ->> dcalendar: if exist => champId
        dcalendar ->> b: confirm, {champId, date}
        b ->> dchamp: if exist
        dchamp ->> b: confirm, {userId}
        b ->> b: if userId = tocken["user"]
        b ->> dcalendar: UPDATE [date, value]
        b ->> c: UPDATE calendar<br>message UPDATE ok
        c->> c: update vue
    end

    rect rgba(150, 0, 0, 0.6)
        note over c,dcalendar:DELETE
        c ->> b: /calendar<br>DELETE<br>[tocken{password_hash,name}<br>id]
        b ->> duser: verif tocken
        duser ->> b: tocken = TRUE
        b ->> dcalendar: if exist => champId
        dcalendar ->> b: confirm, {champId}
        b ->> dchamp: if exist => userId
        dchamp ->> b: confirm, {userId}
        b ->> b: if userId = tocken["user"]
        b ->> dcalendar: DELETE [date, value]
        b ->> c: DELETE calendar<br>message UPDATE ok
        c->> c: update vue
    end
```

[index repo](../README.md)